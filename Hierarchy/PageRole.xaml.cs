﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageRole : ContentPage
    {
        private string _tag;

        public PageRole(string tag)
        {
            _tag = tag;
            Content = null;
        }

        private Label createLabel()
        {
            return new Label
            {
                Text = _tag,
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center,
            };
        }

        private StackLayout createStackLayout(Label label, ListView list)
        {
            StackLayout stackLayout = new StackLayout();

            stackLayout.Children.Add(label);
            stackLayout.Children.Add(list);
            stackLayout.Spacing = 10;
            stackLayout.Orientation = StackOrientation.Vertical;

            return stackLayout;
        }

        public void onClick(object sender, ItemTappedEventArgs e)
        {
            Console.WriteLine(e.Item.ToString());
            Navigation.PushAsync(new ChampPage(e.Item.ToString()));
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            RiotWrapper wrapper = (Application.Current as App).wrapper;
            Dictionary<string, Champion> champions = wrapper.GetChampionsByRole(_tag);
            ListView list = new ListView
            {
                ItemsSource = champions.Keys,
            };
            Label label = createLabel();

            list.SelectionMode = ListViewSelectionMode.Single;
            list.ItemTapped += onClick;
            Content = createStackLayout(label, list);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            (Application.Current as App).lastPage = _tag;
        }
    }
}