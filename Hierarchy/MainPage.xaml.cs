﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hierarchy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void move(object sender, EventArgs e)
        {
            Button btn = (sender as Button);

            await Navigation.PushAsync(new PageRole(btn.ClassId));
        }

        async void moveToLastPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PageRole((Application.Current as App).lastPage));
        }
    }
}
