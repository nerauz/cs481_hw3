﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Linq;
using Newtonsoft.Json;

namespace Hierarchy
{
    public class RiotWrapper
    {
        private HttpClient _client;
        private Dictionary<string, Champion> _champions;
        private string CHAMPIONS_URL = "https://ddragon.leagueoflegends.com/cdn/10.4.1/data/en_US/champion.json";

        public RiotWrapper()
        {
            _client = new HttpClient();
            _getChampions();
        }

        private async void _getChampions()
        {
            HttpResponseMessage response = await _client.GetAsync(CHAMPIONS_URL);
            string json = await response.Content.ReadAsStringAsync();
            RiotAnswer deserialized = JsonConvert.DeserializeObject<RiotAnswer>(json);

            _champions = deserialized.data;
        }

        public Dictionary<string, Champion> GetChampionsByRole(string tag)
        {
            Dictionary<string, Champion> champions = new Dictionary<string, Champion>();

            foreach (KeyValuePair<string, Champion> entry in _champions)
            {
                string[] current_tags = entry.Value.tags;

                if (current_tags.Contains(tag))
                {
                    champions[entry.Key] = entry.Value;
                }
            }

            return champions;
        }
    }
}
