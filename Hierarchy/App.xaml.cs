﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    public partial class App : Application
    {
        public RiotWrapper wrapper;
        public string lastPage;
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
            wrapper = new RiotWrapper();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
