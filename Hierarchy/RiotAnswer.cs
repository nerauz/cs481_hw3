﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hierarchy
{
    public class Champion
    {
        public string version { get; set; }
        public string id { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string blurb { get; set; }
        public object info { get; set; }
        public object image { get; set; }
        public string[] tags { get; set; }
        public string partype { get; set; }
        public object stats { get; set; }

    }
    public class RiotAnswer
    {
        public string type { get; set; }
        public string format { get; set; }
        public string version { get; set; }
        public Dictionary<string, Champion> data { get; set; }
    }
}
